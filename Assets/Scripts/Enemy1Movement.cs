﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy1Movement : MonoBehaviour {

	public float velocity;
	public List<Sprite> sprites;
	public AudioSource hit;

	private SpriteRenderer renderer;
	private CharacterController2D controller;
	private Vector3 direction;

	// Use this for initialization
	void Start () {
		controller = GetComponent<CharacterController2D> ();
		renderer = GetComponent<SpriteRenderer>();
		StartCoroutine (Move());
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag ("Weapon")) {
			hit.Play();
			Destroy (gameObject);	
		}
	}

	IEnumerator Move(){
		float timer = 0f;
		int rand = Random.Range (0, 4);
		switch (rand) {
			case 0:
				direction = Vector3.left;
			renderer.sprite = sprites[2];
			renderer.flipX = false;
				break;
			case 1:
				direction = Vector3.right;
			renderer.sprite = sprites[2];
			renderer.flipX = true;
				break;
			case 2:
				direction = Vector3.up;
			renderer.sprite = sprites[0];
			renderer.flipX = false;
				break;
			case 3:
				direction = Vector3.down;
			renderer.sprite = sprites[1];
			renderer.flipX = false;
				break;
			default:
				break;
		}
		while (timer < Random.Range(0.5f, 1.2f)) {
			controller.move (direction * Time.deltaTime * velocity);
			timer += Time.deltaTime;
			yield return new WaitForSeconds (Time.deltaTime);
		}
		yield return new WaitForSeconds (0.5f);
		StartCoroutine (Move ());
	}
}
