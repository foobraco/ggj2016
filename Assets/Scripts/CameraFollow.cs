﻿using UnityEngine;
using System.Collections;
using System;

public class CameraFollow : MonoBehaviour {

	public float interpVelocity;
	public float minDistance;
	public float followDistance;
	public GameObject followedObject;
	public Vector3 offset;
	Vector3 targetPos;


	// Use this for initialization
	void Start () {
		targetPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (followedObject)
		{
			Vector3 posNoZ = transform.position;
			posNoZ.z = followedObject.transform.position.z;

			Vector3 targetDirection = (followedObject.transform.position - posNoZ);

			interpVelocity = targetDirection.magnitude * 5f;

			targetPos = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime); 

			transform.position = Vector3.Lerp( transform.position, targetPos + offset, 0.25f);

		}

	}
}
