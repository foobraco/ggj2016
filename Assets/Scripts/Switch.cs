﻿using UnityEngine;
using System.Collections;

public class Switch : MonoBehaviour {

	public GameObject door;
	public Sprite pressed;

	private bool isPressed = false;

	void OnTriggerEnter2D(Collider2D coll)
	{
		Debug.LogWarning(coll.name);
		if(coll.CompareTag("Player"))
		{
			Destroy(door);
			GetComponent<SpriteRenderer>().sprite = pressed;
			isPressed = true;
		}
	}
}
