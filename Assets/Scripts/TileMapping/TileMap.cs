﻿namespace Game.TileMapping.Unity
{
    using UnityEngine;
    using System.Collections.Generic;

    /// <summary>
    /// Provides a component for tile mapping.
    /// </summary>

    public class TileMap : MonoBehaviour
    {

        /// <summary>
        /// Gets or sets the number of rows of tiles.
        /// </summary>
        public int Rows;

        /// <summary>
        /// Gets or sets the number of columns of tiles.
        /// </summary>
        public int Columns;

        /// <summary>
        /// Gets or sets the value of the tile width.
        /// </summary>
        public float TileWidth = 10;

        /// <summary>
        /// Gets or sets the value of the tile height.
        /// </summary>
        public float TileHeight = 10f;
        GameObject player;
        public GameObject tile;
        public GameObject portalToSpawn;
        public List<Vector2> waypoints;
        public Vector2 Offsets;

        /// <summary>
        /// Used by editor components or game logic to indicate a tile location.
        /// </summary>
        /// <remarks>This will be hidden from the inspector window. See <see cref="HideInInspector"/></remarks>
        [HideInInspector]
        public Vector3
            MarkerPosition;
        [HideInInspector]
        public Vector3
            startSelection;
        [HideInInspector]
        public Vector3
            endSelection;
        [HideInInspector]
        public bool
        selectionStarted;

        /// <summary>
        /// Initializes a new instance of the <see cref="TileMap"/> class.
        /// </summary>
        public TileMap()
        {
            this.Columns = 20;
            this.Rows = 10;
        }

        void Awake()
        {
            Invoke("SearchForPlayer", 0.5f);
        }

        void SearchForPlayer()
        {
            player = (GameObject.FindGameObjectWithTag("Player"));

        }


        /// <summary>
        /// When the game object is selected this will draw the grid
        /// </summary>
        /// <remarks>Only called when in the Unity editor.</remarks>
        private void OnDrawGizmosSelected()
        {

            // store map width, height and position
            var mapWidth = this.Columns * this.TileWidth;
            var mapHeight = this.Rows * this.TileHeight;
            var position = this.transform.position;

            
            // draw layer border
            Gizmos.color = Color.white;
            Gizmos.DrawLine(position, position + new Vector3(mapWidth, 0, 0));
            Gizmos.DrawLine(position, position + new Vector3(0, mapHeight, 0));
            Gizmos.DrawLine(position + new Vector3(mapWidth, 0, 0), position + new Vector3(mapWidth, mapHeight, 0));
            Gizmos.DrawLine(position + new Vector3(0, mapHeight, 0), position + new Vector3(mapWidth, mapHeight, 0));

            // draw tile cells
            Gizmos.color = Color.grey;
            for (float i = 1; i < this.Columns; i++)
            {
                Gizmos.DrawLine(position + new Vector3(i * this.TileWidth, 0, 0), position + new Vector3(i * this.TileWidth, mapHeight, 0));
            }
            
            for (float i = 1; i < this.Rows; i++)
            {
                Gizmos.DrawLine(position + new Vector3(0, i * this.TileHeight, 0), position + new Vector3(mapWidth, i * this.TileHeight, 0));
            }

            for (int i = 0; i < waypoints.Count; i++)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawCube(new Vector3(waypoints [i].x * TileWidth + TileWidth / 2, waypoints [i].y * TileHeight + TileHeight / 2, 0), new Vector3(TileWidth, TileHeight, 0));
            }

            // Draw marker position
            Gizmos.color = Color.red;
            if (tile)
            {
                SpriteRenderer sRenderer = tile.GetComponent<SpriteRenderer>();
                Texture texture = sRenderer.sprite.texture;
                Rect rect = sRenderer.sprite.rect;
                float pixelSize = sRenderer.sprite.pixelsPerUnit;
                GUI.DrawTexture(new Rect(new Vector2(10, 10), new Vector2(this.TileWidth, this.TileHeight)), texture);
            }
                Gizmos.DrawWireCube(this.MarkerPosition, new Vector3(this.TileWidth, this.TileHeight, 1) * 1.1f);
                        
            if (selectionStarted)
            {
                Gizmos.color = Color.green;
                Vector3 LeftBound = new Vector3(startSelection.x, endSelection.y, 0);
                Gizmos.DrawLine(startSelection, LeftBound);
                LeftBound = new Vector3(endSelection.x, startSelection.y, 0);
                Gizmos.DrawLine(startSelection, LeftBound);
                LeftBound = new Vector3(endSelection.x, startSelection.y, 0);
                Gizmos.DrawLine(endSelection, LeftBound);
                LeftBound = new Vector3(startSelection.x, endSelection.y, 0);
                Gizmos.DrawLine(endSelection, LeftBound);
            }
        }
    }
}
