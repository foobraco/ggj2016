﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class TotemManager : MonoBehaviour {

	[System.Serializable]
	public class Totem
	{
		public Image icon;
		public GameObject totem;
		public bool pickedUp;
	}

	public List<Totem> totems;
	public GameObject finalDoor;
	public int pickedTotems;
	public AudioClip music;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(pickedTotems >= 4)
		{
			Destroy(finalDoor);
		}
	}

	public void totemPickedUp(GameObject picked)
	{
		foreach(Totem t in totems)
		{
			if(picked == t.totem && !t.pickedUp)
			{
				Destroy(t.totem);
				t.pickedUp = true;
				t.icon.enabled = true;
				pickedTotems++;
			}
		}
	}
}
