﻿using UnityEngine;
using System.Collections;

public class ElipticMovement : MonoBehaviour {

	public float velocity;

	private CharacterController2D controller;
	private Vector3 direction;

	// Use this for initialization
	void Start () {
		controller = GetComponent<CharacterController2D> ();	
		StartCoroutine (Move());
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag ("Weapon")) {
			Destroy (gameObject);
		}
	}

	IEnumerator Move(){
		float timer = 0f;
		float angle = 0f;
		float radius = 1f;
		while (timer < 5f) {
			angle = Mathf.Lerp(angle, 90f, 2f* Time.deltaTime);
			transform.position = transform.position + new Vector3(Mathf.Cos(angle) * radius, Mathf.Sin(angle) * radius) * Time.deltaTime;
			timer += Time.deltaTime;
			yield return new WaitForSeconds (Time.deltaTime);
		}
//		timer = 0f;
//		angle = 0f;
//		angle = Mathf.Lerp(angle, 45, 2f* Time.deltaTime);
//		while (timer < 5f) {
//			transform.position = transform.position + new Vector3(Mathf.Cos(angle) * radius, Mathf.Sin(angle) * radius) * velocity * Time.deltaTime;
//			timer += Time.deltaTime;
//			yield return new WaitForSeconds (Time.deltaTime);
//		}
		yield return StartCoroutine (Move ());
	}
}
