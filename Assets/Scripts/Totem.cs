﻿using UnityEngine;
using System.Collections;

public class Totem : MonoBehaviour {

	public TotemManager manager;

	void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{
			manager.totemPickedUp(gameObject);
		}	
	}
}
