﻿using UnityEngine;
using System.Collections;

public class DandelionMovement : MonoBehaviour {

	public enum DandelionStates{
		STOPPED,
		MOVING
	}

	public float verticalTime = 1f;
	public float horizontalVelocity = 0.5f;

	bool floatup = false;
	Vector3 temporalPos;


	void Start(){
		InvokeRepeating("changeYDirection",verticalTime, verticalTime);
	}

	void Update(){
		if(floatup)
		{
			floatingup();
		}else{
			floatingdown();
		}

		moveFoward();
	}

	void moveFoward()
	{
		temporalPos = transform.position;
		temporalPos.z += horizontalVelocity * Time.deltaTime;
		transform.position = temporalPos;
	}

	void changeYDirection()
	{
		floatup = floatup ? false : true;
	}

	void floatingup(){
		temporalPos = transform.position;
		temporalPos.y += 0.3f * Time.deltaTime;
		transform.position = temporalPos;

	}

	void floatingdown(){
		temporalPos = transform.position;
		temporalPos.y -= Random.Range(0.1f, 0.3f)  * Time.deltaTime;
		transform.position = temporalPos;
	}
}
