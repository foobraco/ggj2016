﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DiagonalMovement : MonoBehaviour {

	public float velocity;
	public List<Sprite> sprites;

	private CharacterController2D controller;
	private Vector3 direction;
	private Transform playerPosition;
	public AudioSource hit;

	// Use this for initialization
	void Start () {
		controller = GetComponent<CharacterController2D> ();
		playerPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
	}

	// Update is called once per frame
	void Update () {
		if(Mathf.Abs(playerPosition.position.x - transform.position.x) < 10
			&& Mathf.Abs(playerPosition.position.y - transform.position.y) < 10)
		{
			if(playerPosition.position.x < transform.position.x)
			{
				controller.move(Vector3.left * Time.deltaTime * velocity);
				GetComponent<SpriteRenderer>().flipX = false;
			}else
			{
				controller.move(Vector3.right * Time.deltaTime * velocity);
				GetComponent<SpriteRenderer>().flipX = true;

			}

			if(playerPosition.position.y < transform.position.y)
			{
				controller.move(Vector3.down * Time.deltaTime * velocity);
			}else
			{
				controller.move(Vector3.up * Time.deltaTime * velocity);
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag ("Weapon")) {
			hit.Play();
			Destroy (gameObject);
		}
	}


}
