﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CharacterMovement : MonoBehaviour {

	private enum Direction
	{
		LEFT,
		RIGHT,
		UP,
		DOWN
	}

	public float velocityMultiplier;
	public GameObject weapon;
	public List<Image> hearts;
	public Animator anim;

	Rigidbody2D rigidbody;
	CharacterController2D controller;
	Direction direction;
	private int hitCounter = 0;
	bool isAttacking = false;


	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody2D>();
		controller = GetComponent<CharacterController2D>();
		anim = GetComponent<Animator>();
		direction = Direction.DOWN;
	}
	
	// Update is called once per frame
	void Update () {
		keyboardController();
	}

	void keyboardController()
	{
		if(Input.GetKey(KeyCode.LeftArrow))
		{
			controller.move(Vector3.left * Time.deltaTime * velocityMultiplier);
			direction = Direction.LEFT;
			anim.Play("Hero_Walk");
		}
		if(Input.GetKey(KeyCode.DownArrow))
		{
			controller.move(Vector3.down * Time.deltaTime * velocityMultiplier);
			direction = Direction.DOWN;
			anim.Play("Hero_Walk");
		}
		if(Input.GetKey(KeyCode.RightArrow))
		{
			controller.move(Vector3.right * Time.deltaTime * velocityMultiplier);
			direction = Direction.RIGHT;
			anim.Play("Hero_Walk");
		}
		if(Input.GetKey(KeyCode.UpArrow))
		{
			controller.move(Vector3.up * Time.deltaTime * velocityMultiplier);
			direction = Direction.UP;
			anim.Play("Hero_Walk");
		}

		if(Input.GetKey(KeyCode.Z) && isAttacking == false)
		{
			if(direction.Equals(Direction.LEFT))
			{
				StartCoroutine(attack(Vector3.left));
				anim.Play("Hero_Atack_Side");
				GetComponent<SpriteRenderer>().flipX = true;
			}
			if(direction.Equals(Direction.RIGHT))
			{
				StartCoroutine(attack(Vector3.right));
				anim.Play("Hero_Walk");
				anim.Play("Hero_Atack_Side");
			}
			if(direction.Equals(Direction.UP))
			{
				StartCoroutine(attack(Vector3.up));
				anim.Play("Hero_Walk");
				anim.Play("Hero_Atack_Vertical");
				GetComponent<SpriteRenderer>().flipY = true;
			}
			if(direction.Equals(Direction.DOWN))
			{
				StartCoroutine(attack(Vector3.down));
				anim.Play("Hero_Atack_Vertical");
			}
		}
	}

	IEnumerator attack(Vector3 direction)
	{
		isAttacking = true;

		weapon.SetActive(true);
		float timer = 0f;
		while(timer < 0.1f)
		{
			weapon.transform.localPosition = Vector3.MoveTowards(weapon.transform.localPosition, direction, Time.deltaTime * 5f);
			timer += Time.deltaTime;
			yield return null;
		}
		timer = 0f;
		while(timer < 0.1f)
		{
			weapon.transform.localPosition = Vector3.MoveTowards(weapon.transform.localPosition, Vector3.zero, Time.deltaTime * 5f);
			timer += Time.deltaTime;
			yield return null;
		}
		isAttacking = false;
		weapon.SetActive(false);
		GetComponent<SpriteRenderer>().flipX = false;
		GetComponent<SpriteRenderer>().flipY = false;
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.collider.CompareTag("Enemy"))
		{
			if(hitCounter == 2)
			{
				hitCounter++;
				Destroy(hearts[0].gameObject);
				Application.LoadLevel("TopDownGame");
			}
			if(hitCounter == 1)
			{
				hitCounter++;
				Destroy(hearts[1].gameObject);
			}
			if(hitCounter == 0)
			{
				hitCounter++;
				Destroy(hearts[2].gameObject);
			}



			StartCoroutine(damageAnimation());
		}
	}

	IEnumerator damageAnimation()
	{
		float timer = 0f;
		SpriteRenderer renderer = GetComponent<SpriteRenderer>();
		while(timer < 0.8f)
		{
			renderer.enabled = renderer.enabled ? false : true;
			yield return new WaitForSeconds(0.2f);
			timer += 0.2f;
		}
	}
}
